Calibration routine:


```
#!c

int32_t calibrateScale(uint16_t std_weight)
{

    // TODO: Write cal info to EEPROM

    int32_t cal_raw;    // Temp var for reading
    int32_t cal_factor; // Calibration factor

    writeString("Remove Items From The Scale\n");
    writeString("Press '1' When Finished\n");

    _delay_ms(2000);

    HX711_setScale(1); // Reset the scale parameter
    HX711_tare(100); // Default value. Tare so many times to reduce influence of noise

    writeString("Place 10lbs On Scale\n");
    writeString("Press '1' When Finished\n");
    //while(getByte() != ACCEPT_COMMAND); // Wait for user input. Nothing better to do

    _delay_ms(5000);

    cal_raw = HX711_getUnits(100);
    cal_factor = cal_raw / 10;

    HX711_setScale(cal_factor);
    // EEPROM_set cal_factor

    writeString("Calibration Complete\n");

    return (cal_factor);
}
```